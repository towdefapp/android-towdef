/**
 * Created by Levin Häuser on 21.03.2018.
 */

public class Player {

    private int money;
    private double score;
    private String name;

    public Player(String name) {
        this.name = name;
        money= 0;
        score= 0;
    }

    public void buy (int price){
        money -=price;
    }

    public void increaseMoney (int value){
        money += value;
    }

    //Getter und Setter
    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
